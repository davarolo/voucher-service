# Voucher Service #

## Table of contents ##
+ [Overview](#overview)
+ [Technologies](#technologies)
+ [Setup](#setup)

## Overview ##
The following project contains MercadoLibre Service to validate coupons.
+ [Rest Documentation](http://34.125.36.67:9090/swagger-ui.html)

## Technologies ##
This project is created with:
+ Java 11
+ SpringBoot 2.6.2

## Setup ##
To run this project locally:

1. Clone this repository
2. Execute `gradle clean` on root directory
3. Execute `Gradle build` on root directory
4. Execute `java -jar service-1.0.0.jar` on build/libs
