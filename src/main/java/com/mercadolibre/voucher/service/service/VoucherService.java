package com.mercadolibre.voucher.service.service;

import com.mercadolibre.voucher.service.dto.CouponRequest;
import com.mercadolibre.voucher.service.dto.CouponResponse;
import java.util.List;
import java.util.Map;

public interface VoucherService {

  List<String> calculate(Map<String, Float> items, Float amount);

  CouponResponse coupon(CouponRequest request);

}
