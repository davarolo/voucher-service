package com.mercadolibre.voucher.service.service;

import com.mercadolibre.voucher.service.client.MercadoLibreClient;
import com.mercadolibre.voucher.service.dto.CouponRequest;
import com.mercadolibre.voucher.service.dto.CouponResponse;
import com.mercadolibre.voucher.service.enums.ErrorCode;
import com.mercadolibre.voucher.service.client.exception.MercadoLibreException;
import com.mercadolibre.voucher.service.exception.VoucherException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class VoucherServiceImpl implements VoucherService {

  private final MercadoLibreClient client;

  @Override
  public List<String> calculate(Map<String, Float> items, Float amount) {
    var result = validate(items, amount);
    return result.getItemIds();
  }

  private CouponResponse validate(Map<String, Float> items, Float amount) {
    List<String> recommended = new ArrayList<>();
    var used = 0F;

    var sortedItems =
        items.entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toList());

    for (var item : sortedItems) {
      if ((used + item.getValue()) <= amount) {
        used += item.getValue();
        recommended.add(item.getKey());
      }
    }

    if (used == 0f) {
      throw new VoucherException("Amount not used", ErrorCode.NOT_FOUND);
    }

    recommended.sort(Comparator.naturalOrder());
    return CouponResponse.builder().total(used).itemIds(recommended).build();
  }

  @Override
  public CouponResponse coupon(CouponRequest request) {
    Map<String, Float> items = new HashMap<>();

    try {
      for (var itemId : request.getItemIds()) {
        var item = client.getItem(itemId);
        items.put(item.getId(), item.getPrice());
      }
    } catch (MercadoLibreException me) {
      throw new VoucherException(
          me.getError().getMessage(), me, ErrorCode.MERCADO_LIBRE_EXCEPTION);
    } catch (Exception e) {
      log.error("An unexpected exception has been occurred", e);
      throw new VoucherException(
          "An unexpected exception has been occurred", e, ErrorCode.UNEXPECTED_EXCEPTION);
    }

    return validate(items, request.getAmount());
  }
}
