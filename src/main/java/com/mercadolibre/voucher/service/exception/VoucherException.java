package com.mercadolibre.voucher.service.exception;

import com.mercadolibre.voucher.service.enums.ErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class VoucherException extends RuntimeException {

  private ErrorCode errorCode;

  public VoucherException(String message, ErrorCode errorCode) {
    super(message);
    this.errorCode = errorCode;
  }

  public VoucherException(String message, Throwable throwable, ErrorCode errorCode) {
    super(message, throwable);
    this.errorCode = errorCode;
  }

}
