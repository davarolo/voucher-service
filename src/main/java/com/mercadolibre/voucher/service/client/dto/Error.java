package com.mercadolibre.voucher.service.client.dto;

import lombok.Data;

@Data
public class Error {

  private String message;
  private String error;
  private int status;
  private Object cause;

}
