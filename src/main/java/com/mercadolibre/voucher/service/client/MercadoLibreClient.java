package com.mercadolibre.voucher.service.client;

import com.google.gson.Gson;
import com.mercadolibre.voucher.service.client.dto.Error;
import com.mercadolibre.voucher.service.client.dto.ItemResponse;
import com.mercadolibre.voucher.service.client.exception.MercadoLibreException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MercadoLibreClient {

  @Value("${mercado-libre.items.path}")
  private String itemsPath;

  private final static Gson gson = new Gson();

  @Cacheable(value = "items")
  public ItemResponse getItem(String itemId) throws Exception {
    String url = itemsPath + itemId;
    HttpGet get = new HttpGet(url);
    var response = executeHttp(get);
    return gson.fromJson(response, ItemResponse.class);
  }

  private String executeHttp(HttpUriRequest request) throws Exception {
    String result = null;

    HttpClient httpClient =
        HttpClients.custom().build();

    HttpResponse response = httpClient.execute(request);

    HttpEntity entity = response.getEntity();

    if (entity != null) {
      result = EntityUtils.toString(entity);
    }

    if (response.getStatusLine().getStatusCode() < 200
        || response.getStatusLine().getStatusCode() > 300) {
      log.error(
          "Result [uri={}, code={}, message={}]",
          request.getURI().toString(),
          response.getStatusLine().getStatusCode(),
          result);

      var error = gson.fromJson(result, Error.class);
      throw new MercadoLibreException("An unexpected exception has been occurred", error);
    }

    return result;
  }

}
