package com.mercadolibre.voucher.service.client.dto;

import lombok.Data;

@Data
public class ItemResponse {

  private String id;
  private String title;
  private Float price;
  private String siteId;

}
