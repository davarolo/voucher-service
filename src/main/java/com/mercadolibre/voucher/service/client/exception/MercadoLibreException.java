package com.mercadolibre.voucher.service.client.exception;

import com.mercadolibre.voucher.service.client.dto.Error;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MercadoLibreException extends Exception {

  private Error error;

  public MercadoLibreException(String message, Error error) {
    super(message);
    this.error = error;
  }

}
