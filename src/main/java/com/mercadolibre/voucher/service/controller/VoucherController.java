package com.mercadolibre.voucher.service.controller;

import com.mercadolibre.voucher.service.dto.CouponRequest;
import com.mercadolibre.voucher.service.dto.CouponResponse;
import com.mercadolibre.voucher.service.dto.ErrorResponse;
import com.mercadolibre.voucher.service.service.VoucherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class VoucherController {

  private final VoucherService service;

  @Operation(summary = "Validate amount and items")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Coupon is valid",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = CouponResponse.class))
            }),
        @ApiResponse(
            responseCode = "404",
            description = "Amount is less than items",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ErrorResponse.class))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = ErrorResponse.class))
            })
      })
  @PostMapping("/coupon")
  private CouponResponse coupon(@RequestBody CouponRequest request) {
    return service.coupon(request);
  }
}
