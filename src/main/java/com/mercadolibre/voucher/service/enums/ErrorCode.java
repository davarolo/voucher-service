package com.mercadolibre.voucher.service.enums;

public enum ErrorCode {

  NOT_FOUND(404),
  MERCADO_LIBRE_EXCEPTION(500),
  UNEXPECTED_EXCEPTION(500);

  private final int status;

  ErrorCode(int status) {
    this.status = status;
  }

  public int getStatus() {
    return this.status;
  }
}
