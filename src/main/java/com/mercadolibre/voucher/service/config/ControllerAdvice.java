package com.mercadolibre.voucher.service.config;

import com.mercadolibre.voucher.service.dto.ErrorResponse;
import com.mercadolibre.voucher.service.exception.VoucherException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice extends ResponseEntityExceptionHandler {

  @ExceptionHandler(VoucherException.class)
  public ResponseEntity<ErrorResponse> handleVoucherException(VoucherException ex) {

    return new ResponseEntity<>(
        ErrorResponse.builder()
            .status(ex.getErrorCode().getStatus())
            .code(ex.getErrorCode().name())
            .message(ex.getMessage())
            .build(),
        HttpStatus.valueOf(ex.getErrorCode().getStatus()));
  }
}
