package com.mercadolibre.voucher.service.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class ErrorResponse {

  private int status;
  private String code;
  private String message;

}
