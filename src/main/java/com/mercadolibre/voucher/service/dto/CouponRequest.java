package com.mercadolibre.voucher.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class CouponRequest {

  @JsonProperty("item_ids")
  private List<String> itemIds;
  private Float amount;

}
