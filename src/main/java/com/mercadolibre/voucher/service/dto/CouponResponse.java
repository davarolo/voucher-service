package com.mercadolibre.voucher.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class CouponResponse {

  @JsonProperty("item_ids")
  private List<String> itemIds;
  private Float total;

}
