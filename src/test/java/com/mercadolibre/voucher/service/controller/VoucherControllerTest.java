package com.mercadolibre.voucher.service.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.voucher.service.dto.CouponRequest;
import com.mercadolibre.voucher.service.dto.CouponResponse;
import com.mercadolibre.voucher.service.enums.ErrorCode;
import com.mercadolibre.voucher.service.exception.VoucherException;
import com.mercadolibre.voucher.service.service.VoucherService;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = VoucherController.class)
@AutoConfigureMockMvc(addFilters = false)
public class VoucherControllerTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private VoucherService service;

  @Autowired private ObjectMapper objectMapper;

  private final String COUPON_PATH = "/coupon";
  private final String CONTENT_TYPE = "application/json";

  @Test
  void whenCouponIsValid_ThenReturns200() throws Exception {
    Mockito.when(service.coupon(Mockito.any())).thenReturn(buildResponse());

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(COUPON_PATH)
                .contentType(CONTENT_TYPE)
                .content(buildRequest()))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void whenAmountIsLessThanItems_ThenReturns404() throws Exception {
    Mockito.when(service.coupon(Mockito.any()))
        .thenThrow(new VoucherException("Amount not used", ErrorCode.NOT_FOUND));

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(COUPON_PATH)
                .contentType(CONTENT_TYPE)
                .content(buildRequest()))
        .andExpect(MockMvcResultMatchers.status().is4xxClientError());
  }

  @Test
  void whenItemIsNotFoundOnMercadoLibre_ThenReturns500() throws Exception {
    Mockito.when(service.coupon(Mockito.any()))
        .thenThrow(
            new VoucherException("Item with id MLA1 not found.", ErrorCode.MERCADO_LIBRE_EXCEPTION));

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(COUPON_PATH)
                .contentType(CONTENT_TYPE)
                .content(buildRequest()))
        .andExpect(MockMvcResultMatchers.status().is5xxServerError());
  }

  private String buildRequest() throws JsonProcessingException {
    var itemIds = Arrays.asList("MLA1", "MLA2", "MLA3", "MLA4", "MLA5");

    var request = new CouponRequest();
    request.setItemIds(itemIds);
    request.setAmount(500F);

    return objectMapper.writeValueAsString(request);
  }

  private CouponResponse buildResponse() {
    return CouponResponse.builder()
        .itemIds(Arrays.asList("MLA1", "MLA2", "MLA4", "MLA5"))
        .total(480F)
        .build();
  }
}
