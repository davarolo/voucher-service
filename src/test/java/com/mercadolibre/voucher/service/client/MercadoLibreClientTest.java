package com.mercadolibre.voucher.service.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.voucher.service.client.dto.Error;
import com.mercadolibre.voucher.service.client.dto.ItemResponse;
import com.mercadolibre.voucher.service.client.exception.MercadoLibreException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(SpringExtension.class)
public class MercadoLibreClientTest {

  @InjectMocks private MercadoLibreClient client;

  private final ObjectMapper objectMapper = new ObjectMapper();

  private MockWebServer mockWebServer;

  @BeforeEach
  void setUp() {
    this.mockWebServer = new MockWebServer();
    ReflectionTestUtils.setField(client, "itemsPath", mockWebServer.url("/").toString());
  }

  @Test
  void whenExecuteGetItem_thenReturnResponseItem() throws Exception {
    mockWebServer.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json; charset-utf-8")
            .setBody(buildItemResponse())
            .setResponseCode(200));

    var response = client.getItem("MLA1");

    Assertions.assertEquals("MLA1", response.getId());
    Assertions.assertEquals("MLA", response.getSiteId());
    Assertions.assertEquals("Test Item", response.getTitle());
    Assertions.assertEquals(100f, response.getPrice());
  }

  @Test
  void whenExecuteGetItemAndItemNotExistsOnMercadoLibre_thenThrowsMercadoLibreException()
      throws Exception {
    mockWebServer.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json; charset-utf-8")
            .setBody(buildErrorResponse())
            .setResponseCode(404));

    Assertions.assertThrows(MercadoLibreException.class, () -> client.getItem("MLA1"));
  }

  private String buildItemResponse() throws JsonProcessingException {
    ItemResponse itemResponse = new ItemResponse();
    itemResponse.setId("MLA1");
    itemResponse.setPrice(100F);
    itemResponse.setSiteId("MLA");
    itemResponse.setTitle("Test Item");

    return objectMapper.writeValueAsString(itemResponse);
  }

  private String buildErrorResponse() throws JsonProcessingException {
    Error error = new Error();
    error.setError("not_found");
    error.setCause(new String[] {});
    error.setStatus(404);
    error.setMessage("Item with id MLA1 not found");

    return objectMapper.writeValueAsString(error);
  }
}
