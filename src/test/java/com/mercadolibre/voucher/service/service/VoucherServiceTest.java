package com.mercadolibre.voucher.service.service;

import com.mercadolibre.voucher.service.client.MercadoLibreClient;
import com.mercadolibre.voucher.service.client.dto.Error;
import com.mercadolibre.voucher.service.client.dto.ItemResponse;
import com.mercadolibre.voucher.service.client.exception.MercadoLibreException;
import com.mercadolibre.voucher.service.dto.CouponRequest;
import com.mercadolibre.voucher.service.exception.VoucherException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class VoucherServiceTest {

  @InjectMocks private VoucherServiceImpl service;

  @Mock private MercadoLibreClient client;

  @Test
  void whenValidateSampleData_thenReturnExpectedResult() {
    var expected = Arrays.asList("MLA1", "MLA2", "MLA4", "MLA5");
    var result = service.calculate(buildSampleItems(), 500F);

    Assertions.assertEquals(expected, result);
  }

  @Test
  void whenAmountIsLessThanItems_thenThrowsVoucherException() {
    Assertions.assertThrows(
        VoucherException.class,
        () -> service.calculate(buildSampleItems(), 50F));
  }

  @Test
  void whenItemsFoundedOnMercadoLibreAndAmountIsMajorThanItems_thenReturnItemIdsAndTotal()
      throws Exception {
    ItemResponse itemResponse = new ItemResponse();
    itemResponse.setId("MLA1");
    itemResponse.setPrice(100F);
    itemResponse.setSiteId("MLA");
    itemResponse.setTitle("Test Item");

    var items = List.of("MLA1");

    CouponRequest request = new CouponRequest();
    request.setAmount(100f);
    request.setItemIds(items);

    Mockito.when(client.getItem(Mockito.anyString())).thenReturn(itemResponse);

    var result = service.coupon(request);

    Assertions.assertEquals(List.of("MLA1"), result.getItemIds());
    Assertions.assertEquals(100f, result.getTotal());
  }

  @Test
  void whenItemIsNotFoundOnMercadoLibre_thenThrowsVoucherException() throws Exception {
    Error error = new Error();
    error.setError("not_found");
    error.setCause(new String[] {});
    error.setStatus(404);
    error.setMessage("Item with id MLA1 not found");

    Mockito.when(client.getItem(Mockito.anyString()))
        .thenThrow(new MercadoLibreException("An unexpected exception has been occurred", error));

    Assertions.assertThrows(
        VoucherException.class,
        () -> service.coupon(buildRequest()));
  }

  @Test
  void whenNullPointerExceptionOnItemId_thenThrowsVoucherException() throws Exception {
    Mockito.when(client.getItem(Mockito.anyString())).thenThrow(new NullPointerException());

    Assertions.assertThrows(
        VoucherException.class,
        () -> service.coupon(buildRequest()));
  }

  private Map<String, Float> buildSampleItems() {
    var items = new HashMap<String, Float>();
    items.put("MLA1", 100f);
    items.put("MLA2", 210f);
    items.put("MLA3", 260f);
    items.put("MLA4", 80f);
    items.put("MLA5", 90f);

    return items;
  }

  private CouponRequest buildRequest() {
    var itemIds = Arrays.asList("MLA1", "MLA2", "MLA3", "MLA4", "MLA5");

    var request = new CouponRequest();
    request.setItemIds(itemIds);
    request.setAmount(500F);

    return request;
  }
}
